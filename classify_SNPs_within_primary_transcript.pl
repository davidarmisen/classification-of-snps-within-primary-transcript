#!/usr/bin/perl
# Script to classify SNPs within primary transcript
# Written by D.Armisen on 19/02/2019. Modifed for publication 10/01/2020
use strict;
use Getopt::Long;
use Storable;

my $verbose=0;


# Create user defined variables
my ($fasta,$temp,$project,$gtf,$gff3,$frames_to_use,$output,$vcf,$help,$store,$use_stored);

# Define some default variable values
$temp=".";
$project="new_project";
$output="$project.snp.classification.vcf";
$fasta="stored";
$gtf="stored";

# Read user defined variables
GetOptions (	"fasta=s" => \$fasta,
		"temp=s" => \$temp,
		"project=s" => \$project,
		"gtf=s" => \$gtf,
		"gff3=s" => \$gff3,
		"output=s" => \$output,
		"vcf=s" => \$vcf, 
		"frames_to_use=s" => \$frames_to_use, 
		"store" => \$store, 
		"use_stored" => \$use_stored, 
		"verbose" => \$verbose,
		"help" => \$help)
or die ("Error in command line arguments\n");

if ($help==1){
	print '# Classification of SNPs within primary transcript
# Written by D.Armisen on 19/02/2019. Modifed for publication 10/01/2020

Perl script that classifies SNPs within primary transcript according to their position (UTR5\', UTR3\', exon or intron) and its impact on protein sequence (synonymous, non-synonymous  or stop).

It needs a sequences fasta file, a GTF and a VCF file. Optionally we can use a GFF3 to parse frames data only.

As outputs we obtain:

*  A VCF file containing original VCF information plus 4 new columns: SNPs position (UTR\'5, UTR\'3, exon or intron), reference codon, variant codon, reference aminoacid, variant aminoacid, synonymous/non-synonymous/stop mutation, frame , case (internal use), transcript name
*  A text file describing all SNPs within CDS with multiple outcomes, i.e. multiple variants
*  A text file describing all SNPs within CDS

Options:
-project = "Project_name" : It will be used to name all the storable and output files. Default: "new_project".
-fasta = "fasta_file.fa" : Path to fasta file containing chromosome sequences. 
-gtf = "gtf.file" : Path to gtf annotation file containing features coordinates. We use GTF format because it clearly distinguishes UTR from coding exons.
-vcf = "vcf.file" : Path to vcf file containing the SNPs we want to classify.
-temp = "temp_folder" : Path to temporary folder where storable data will be writen. It must be created by the user and it must have write permisions. Defaul: Current folder.
-output = "output.vcf" : Output vcf file name. Default: "$project.snp.classification.vcf"

Other options (optional):
-gff3 = "gff3.file" : Path to gff3 file containing features dataframes. Option added because in our case GTF file contained some dataframe errors.
-frames_to_use = gff3|gtf : Define which frames data to use: "gff3" or "gtf"
-store : Decide whether we want to store the intermediate files or not. This will greatly improve reading speed in successive iterations. Recommended if we want to test multiple vcf files.
-use_stored : If the script has already been ran once, all the storable files have been created and we want to use those to test new vcf files.
-verbose : Print extra information
-help : Print this help
';
	exit;
}

# Check if all files exist
if ($use_stored == 0){die "ERROR! The fasta file $fasta does not exist." unless -e "$fasta";}
if ($use_stored == 0){die "ERROR! The gtf file $gtf does not exist." unless -e "$gtf";}
die "ERROR! The folder $temp does not exist." unless -d "$temp";
die "ERROR! The vcf file $vcf does not exist." unless -e "$vcf";
if ($gff3 ne ""){die "ERROR! The gff3 file $gff3 does not exist." unless -e "$gff3";$frames_to_use="gff3";}

if ($gtf eq "stored" && $frames_to_use eq ""){die "ERROR! Please select dataframe information source : 'gtf' or 'gff3'.\n"}


# Define all used variables
my ($x,$y,$z,$r,$s,$t,$cur,$sum,$ref,$alt,$cum,%var,%chr,%gtf,%trans_start,%trans_stop,%genes,%seq,@a,@b,@c,@d,@e,@res1,@res2,@res3,@res4,@res5,%cds,%exon,%utr5,%utr3,%sense,%frame,%split,@dprev,@dnext,$cont,$case,$ktemp,@ktrack,%kall,%kcds,%kutr5,%kutr3,%kexon,%kintron);

# Universal translation code to check for synonymous, nonsynonymous and stop changes. Note: STOP codons tagged as @ instead of *
my %aacode= ( 
"TTT"=>"F","TCT"=>"S","TAT"=>"Y","TGT"=>"C",
"TTC"=>"F","TCC"=>"S","TAC"=>"Y","TGC"=>"C",
"TTA"=>"L","TCA"=>"S","TAA"=>"@","TGA"=>"@",
"TTG"=>"L","TCG"=>"S","TAG"=>"@","TGG"=>"W",
"CTT"=>"L","CCT"=>"P","CAT"=>"H","CGT"=>"R",
"CTC"=>"L","CCC"=>"P","CAC"=>"H","CGC"=>"R",
"CTA"=>"L","CCA"=>"P","CAA"=>"Q","CGA"=>"R",
"CTG"=>"L","CCG"=>"P","CAG"=>"Q","CGG"=>"R",
"ATT"=>"I","ACT"=>"T","AAT"=>"N","AGT"=>"S",
"ATC"=>"I","ACC"=>"T","AAC"=>"N","AGC"=>"S",
"ATA"=>"I","ACA"=>"T","AAA"=>"K","AGA"=>"R",
"ATG"=>"M","ACG"=>"T","AAG"=>"K","AGG"=>"R",
"GTT"=>"V","GCT"=>"A","GAT"=>"D","GGT"=>"G",
"GTC"=>"V","GCC"=>"A","GAC"=>"D","GGC"=>"G",
"GTA"=>"V","GCA"=>"A","GAA"=>"E","GGA"=>"G",
"GTG"=>"V","GCG"=>"A","GAG"=>"E","GGG"=>"G"
);



# Load fasta sequences to extract genes later 
print "Loading chromosomes sequences\n";
if ($use_stored == 0){
	die "ERROR! The file $fasta does not exist." unless -e "$fasta";

	# Linealize first. I could have used other options like sed or awk but I ran on some issues. I think using a perl solution will be slower but safer.
	$x=();
	open (OPEN,"<$fasta");
	while (<OPEN>){
		chomp $_;
		if ($_=~ /^>/){
			if ($x ne ""){$seq{$x}=$y;}
			$_=~ s/>//;
			@_=split(/\s/,$_);
			$x=$_[0];
			$y=();
		}
		elsif ($_=~ /\S/){
			$y=$y.$_;
                }

	}
	if ($x ne ""){$seq{$x}=$y;}
	close (OPEN);
	
        # Store for quicker use later
	if ($store==1){
		store(\%seq, "$temp/$project.seq.perl") or die "Can't store %a in $temp/!\n";
	}
}
else {
        die "ERROR! The file $temp/$project.seq.perl does not exist." unless -e "$temp/$project.seq.perl";
	%seq = %{retrieve("$temp/$project.seq.perl")};
}
print "Chromosomes sequences loaded\n";


# Load all features from gtf. This is easier because it clearly distinguishes UTR from coding exons
print "Loading features\n";
if ($use_stored == 0){
	die "ERROR! The file $gtf does not exist." unless -e "$gtf";

	$x=0;
	
	open (OPEN,"<$gtf");
	while (<OPEN>){
		chomp $_;
		@a=split(/\t/,$_);
		@b=split(/transcript_id \"/,$_);
		@b=split(/\";/,$b[1]);
			
		
		if ($a[2] eq "transcript"){
			$trans_start{$b[0]}=$a[3];
			$trans_stop{$b[0]}=$a[4];
			
			# To gain time afterwards by testing a much smaller subset of genes coordinates but also space by avoiding to keep track of all nucleotide positions
			$sum=int($a[3]/1000);	# To assign each transcript to a window
			if ($split{"$a[0] $sum"} eq undef){$split{"$a[0] $sum"}=$b[0];}
			else {$split{"$a[0] $sum"}=$split{"$a[0] $sum"}." $b[0]";}
			while ($sum<int($a[4]/1000)){
				$sum=$sum+1;
				$split{"$a[0] $sum"}=$split{"$a[0] $sum"}." $b[0]";
			}


			$sense{$b[0]}=$a[6];
			if (!$genes{$a[0]}){$genes{$a[0]}=$b[0];}
			else {$genes{$a[0]}=$genes{$a[0]}." $b[0]";}
		}	
		elsif ($a[2] eq "CDS"){
			if (!$cds{$b[0]}){$cds{$b[0]}="$a[3]-$a[4]";}
               	        else {$cds{$b[0]}=$cds{$b[0]}." $a[3]-$a[4]";}
			$frame{"$b[0] $a[3] $a[4]"}=$a[7];
		}
		elsif ($a[2] eq "exon"){
			if (!$exon{$b[0]}){$exon{$b[0]}="$a[3]-$a[4]";}
                       	else {$exon{$b[0]}=$exon{$b[0]}." $a[3]-$a[4]";}
		}
		elsif ($a[2] eq "3UTR"){
			if (!$utr3{$b[0]}){$utr3{$b[0]}="$a[3]-$a[4]";}
                       	else {$utr3{$b[0]}=$utr3{$b[0]}." $a[3]-$a[4]";}
		}
		elsif ($a[2] eq "5UTR"){
			if (!$utr5{$b[0]}){$utr5{$b[0]}="$a[3]-$a[4]";}
                       	else {$utr5{$b[0]}=$utr5{$b[0]}." $a[3]-$a[4]";}
		}
		if ($verbose==1){$x++;if ($x%10000==0){print " Parsing line $x...\n";}}
	}
	if ($store==1){
        	store(\%genes, "$temp/$project.genes.perl") or die "Can't store %a in $temp/!\n";
	        store(\%sense, "$temp/$project.sense.perl") or die "Can't store %a in $temp/!\n";
	        store(\%frame, "$temp/$project.frame.perl") or die "Can't store %a in $temp/!\n";
	        store(\%split, "$temp/$project.split.perl") or die "Can't store %a in $temp/!\n";
	        store(\%cds, "$temp/$project.cds.perl") or die "Can't store %a in $temp/!\n";
	        store(\%exon, "$temp/$project.exon.perl") or die "Can't store %a in $temp/!\n";
	        store(\%utr3, "$temp/$project.utr3.perl") or die "Can't store %a in $temp/!\n";
	        store(\%utr5, "$temp/$project.utr5.perl") or die "Can't store %a in $temp/!\n";
	        store(\%trans_start, "$temp/$project.trans_start.perl") or die "Can't store %a in $temp/!\n";
	        store(\%trans_stop, "$temp/$project.trans_stop.perl") or die "Can't store %a in $temp/!\n";
	}

	# Load GFF3 with corrected frames. We needed this option as our gtf file contained frame errors
 	if ($frames_to_use eq "gff3"){
		die "ERROR! The file $gff3 does not exist." unless -e "$gff3";

		print "Loading gff3 frames\n";
		%frame=();
		$x=0;
		open (OPEN,"<$gff3");
		while (<OPEN>){
			chomp $_;
			@a=split(/\t/,$_);
			@b=split(/Parent=/,$_);
			@b=split(/;/,$b[1]);
			if ($a[2] eq "CDS"){
				$frame{"$b[0] $a[3] $a[4]"}=$a[7];
			}
		
			if ($verbose==1){$x++;if ($x%10000==0){print " Parsing line $x...\n";}}

		}
		if ($store==1){
		        store(\%frame, "$temp/$project.frame_gff3.perl") or die "Can't store %a in $temp/!\n";
		}
	}
}
else {
	%genes = %{retrieve("$temp/$project.genes.perl")};
	%sense = %{retrieve("$temp/$project.sense.perl")};
	if ($frames_to_use eq "gff3"){%frame = %{retrieve("$temp/$project.frame_gff3.perl")};}
	elsif ($frames_to_use eq "gtf"){%frame = %{retrieve("$temp/$project.frame.perl")};}
	else {die "ERROR! Invalid frames to use selected\n";}
	%split = %{retrieve("$temp/$project.split.perl")};
	%cds = %{retrieve("$temp/$project.cds.perl")};
	%exon = %{retrieve("$temp/$project.exon.perl")};
	%utr3 = %{retrieve("$temp/$project.utr3.perl")};
	%utr5 = %{retrieve("$temp/$project.utr5.perl")};
	%trans_start = %{retrieve("$temp/$project.trans_start.perl")};
	%trans_stop = %{retrieve("$temp/$project.trans_stop.perl")};
	#foreach (keys %cds){print "$_ -> $cds{$_}\n";}
}
print "Features loaded\n";


# Classify now
print "All preliminary data loaded. Start classification\n";
die "ERROR! The file $vcf does not exist." unless -e "$vcf";
open (OPEN,"<$vcf");
open (SAVE,">$output");
while (<OPEN>){
	chomp $_;
	if ($_ =~ /^#/ && $_ !~ /^#CHROM/){
		print SAVE "$_\n";
	}
	elsif ($_ =~ /^#CHROM/){
		@_=split(/\t/,$_);
		pop @_;
		$_=join('\t',$_);
		print SAVE "$_\tFeature\tREFCOD\tALTCOD\tREFAA\tALTAA\tSYNONYMOUS\tFrame\tCase\tTRANSCRIPT\n";
	}
	elsif ($_ !~ /^#/){
		if ($verbose==1){print "$_\n";}
		$y=$_;
		@a=split(/\t/,$_);
		#@b=split(/ /,$genes{$a[0]});
	
		$ktemp=();
		@ktrack=();
		%kall=();
		%kcds=();
		%kutr5=();
		%kutr3=();
		%kexon=();
		%kintron=();
		
		$cur=int($a[1]/1000);	# To recover the SNP window
		@b=split(/ /,$split{"$a[0] $cur"});
		foreach $cur (@b){
			if ($trans_start{$cur}<=$a[1] && $a[1]<=$trans_stop{$cur}){
				if ($verbose==1){print "$cur - $a[1] $trans_start{$cur} $trans_stop{$cur}\n";}
				$z=0;
				$cum=0;
				@c=split(/ /,$cds{$cur});
				for ($cont=0;$cont<=$#c;$cont++){
					if ($z==0){
						@d=split(/-/,$c[$cont]);
						$case=0;
						if ($d[0]<=$a[1] && $a[1]<=$d[1]){
							$z=1;if ($verbose==1){print "CDS $_\n";}						
							

							# Fix recover SNPs very close to intron coordinates
							if ($sense{$cur} eq "+" & $d[1]-$a[1]<2 & $cont<$#c){   # Current---XX X-Next
								if($cont<$#c){@dnext=split(/-/,$c[$cont+1]);}	
								$s=substr $seq{$a[0]}, $a[1]-3 , (3+($d[1]-$a[1]));	# 3 Are the nucleotides that I'll take for sure
								$s=$s.(substr $seq{$a[0]}, $dnext[0]-1 , (2-($d[1]-$a[1])));	# 2 are the maximum number of nucleotides that I'll take if the SNP was at the last position of the feature
								$case=1;
								if ($verbose==1){print "Case 1 + Current---XX X-Next\n";}
							}
							elsif ($sense{$cur} eq "+" & $a[1]-$d[0]<2 & $cont!=0){   # Previous---X XX-Current
        							if($cont!=0){@dprev=split(/-/,$c[$cont-1]);}
               			                                $s=substr $seq{$a[0]}, $d[0]-1 , (3+($a[1]-$d[0]));	# 3 Are the nucleotides that I'll take for sure
                                                                $s=(substr $seq{$a[0]}, $dprev[1]-(2-($a[1]-$d[0])) , (2-($a[1]-$d[0]))).$s;	# 2 are the maximum number of nucleotides that I'll take if the SNP was at the first position of the feature
								$case=2;
								if ($verbose==1){print "Case 2 + Previous---X XX-Current\n";}
                                                        }
							elsif ($sense{$cur} eq "-" & $a[1]-$d[0]<2 & $cont!=0){   # Next-X XX---Current
         							if($cont!=0){@dnext=split(/-/,$c[$cont-1]);}
                                                                $s=substr $seq{$a[0]}, $d[0]-1 , (3+($a[1]-$d[0]));     # 3 Are the nucleotides that I'll take for sure
                                                                $s=(substr $seq{$a[0]}, $dnext[1]-(2-($a[1]-$d[0])) , (2-($a[1]-$d[0]))).$s;      # 2 are the maximum number of nucleotides that I'll take if the SNP was at the last position of the feature
								$case=3;
								if ($verbose==1){print "Case 3 - Next-X XX---Current\n";}
                                                        }
							elsif ($sense{$cur} eq "-" & $d[1]-$a[1]<2 & $cont<$#c){   # Current---X XX-Previous
								if($cont<$#c){@dprev=split(/-/,$c[$cont+1]);}
                                                                $s=substr $seq{$a[0]}, $a[1]-3 , (3+($d[1]-$a[1]));     # 3 Are the nucleotides that I'll take for sure
                                                                $s=$s.(substr $seq{$a[0]}, $dprev[0]-1 , (2-($d[1]-$a[1])));      # 2 are the maximum number of nucleotides that I'll take if the SNP was at the first position of the feature
								$case=4;
								if ($verbose==1){print "Case 4 - Current---X XX-Previous\n";}
                                                        }       
							else {	# This category also include cases where $cont==0 and $cont==$#c but I don't care of taking the wrong nucleotides to complete the 5 nucleotides extraction as posterior frame subtr won't include them
								$s=substr $seq{$a[0]}, $a[1]-3 , 5;
								$case=5;
								if ($verbose==1){print "Case 5\n";}
							}
							if ($verbose==1){print "$a[0] $a[3] -- $a[1] -- $d[0] $d[1]\n$s\n";}
							if ($verbose==1){print "$a[0] $a[3] -- $s $a[1] -- $d[0] $d[1]\n$s\n";}
							$cum=$frame{"$cur $d[0] $d[1]"};
							if ($verbose==1){print "Frame: $cum\n";}
							if ($cum!=0){$cum=3-$cum;}
							if ($verbose==1){print "Nucleotides: $cum\n";}

							if ($sense{$cur} eq "+"){
								$r=($cum+($a[1]-$d[0]))%3;
								if ($verbose==1){print "$r\n";}
							}
							elsif ($sense{$cur} eq "-"){
								$r=($d[1]-$a[1])%3;
								if ($verbose==1){print "$r\n";}
								$r=($cum+$r)%3; 
								if ($verbose==1){print "$r\n";}
								$s=reverse $s;
								$s=~ tr/ATCG/TAGC/;
							}
							if ($verbose==1){print "***$s***\n";}

							$s=uc $s;
									
							if ($r==0){$ref=substr $s, 2 , 3;}
							if ($r==1){$ref=substr $s, 1 , 3;}
							if ($r==2){$ref=substr $s, 0 , 3;}
							if ($verbose==1){print "---$ref\t$aacode{$ref}\n";}
						
							@res1=();@res2=();@res3=();@res4=();@res5=();
							@e=split(/,/,$a[4]);
							
							push @res1,$ref;
#							push @res2,$alt;
							push @res3,$aacode{$ref};
#							push @res4,$aacode{$alt};
#							push @res5,$t;
							
							foreach (@e){
								if ($_ ne "*"){
									if ($sense{$cur} eq "-"){$_=~ tr/ATCG/TAGC/;}

									$alt=$ref;
									if ($r==0){substr $alt, 0 , 1 , $_;}
									if ($r==1){substr $alt, 1 , 1 , $_;}
									if ($r==2){substr $alt, 2 , 1 , $_;}
									if ($verbose==1){print "---$alt\t$aacode{$alt}--\n";}
								
									if ($aacode{$ref} eq $aacode{$alt}){$t="S";if ($verbose==1){print "S\n";}}
									else {$t="N";if ($verbose==1){print "N\n";}}

#									push @res1,$ref;
									push @res2,$alt;
#									push @res3,$aacode{$ref};
									push @res4,$aacode{$alt};
									push @res5,$t;
								}
								else {	
#									push @res1,"*";
									push @res2,"*";
#									push @res3,"*";
									push @res4,"*";
									push @res5,"*";
								}
							}
						}
					}
				}
				if ($z==1){
					$ktemp="$y\tCDS";
					$t=join(',',@res1);$ktemp=$ktemp."\t$t";
					$t=join(',',@res2);$ktemp=$ktemp."\t$t";
					$t=join(',',@res3);$ktemp=$ktemp."\t$t";
					$t=join(',',@res4);$ktemp=$ktemp."\t$t";
					$t=join(',',@res5);$ktemp=$ktemp."\t$t";
					$ktemp=$ktemp."\t$r\t$case";

					if (!$kall{$ktemp}){$kall{$ktemp}=1;push @ktrack,$ktemp;}
					if (!$kcds{$ktemp}){$kcds{$ktemp}=$cur;}
					else {$kcds{$ktemp}="$kcds{$ktemp},$cur";}
				}
				if ($z==0){
					@c=split(/ /,$utr5{$cur});
					foreach (@c){
						@d=split(/-/,$_);
						if ($d[0]<=$a[1] && $a[1]<=$d[1]){$z=1;if ($verbose==1){print "UTR5 $_\n";}}
					}
					if ($z==1){
                                                $ktemp="$y\tUTR5\t.\t.\t.\t.\t.\t.\t.";

						if (!$kall{$ktemp}){$kall{$ktemp}=1;push @ktrack,$ktemp;}
						if (!$kutr5{$ktemp}){$kutr5{$ktemp}=$cur;}
						else {$kutr5{$ktemp}="$kutr5{$ktemp},$cur";}
                                        }
				}
				if ($z==0){
					@c=split(/ /,$utr3{$cur});
					foreach (@c){
						@d=split(/-/,$_);
						if ($d[0]<=$a[1] && $a[1]<=$d[1]){$z=1;if ($verbose==1){print "UTR3 $_\n";}}
					}
					if ($z==1){
                                                $ktemp="$y\tUTR3\t.\t.\t.\t.\t.\t.\t.";

						if (!$kall{$ktemp}){$kall{$ktemp}=1;push @ktrack,$ktemp;}
						if (!$kutr5{$ktemp}){$kutr5{$ktemp}=$cur;}
                                                if (!$kutr3{$ktemp}){$kutr3{$ktemp}=$cur;}
                                                else {$kutr3{$ktemp}="$kutr3{$ktemp},$cur";}
                                        }
				}
				if ($z==0){
					@c=split(/ /,$exon{$cur});
					foreach (@c){
						@d=split(/-/,$_);
						if ($d[0]<=$a[1] && $a[1]<=$d[1]){$z=1;if ($verbose==1){print "Exon $_\n";}}
					}
					if ($z==1){
                                                $ktemp="$y\tExon\t.\t.\t.\t.\t.\t.\t.";

						if (!$kall{$ktemp}){$kall{$ktemp}=1;push @ktrack,$ktemp;}
                                                if (!$kexon{$ktemp}){$kexon{$ktemp}=$cur;}
                                                else {$kexon{$ktemp}="$kexon{$ktemp},$cur";}
					}
				}
				if ($z==0){
					if ($verbose==1){print "Intron $_\n";}
					$ktemp="$y\tIntron\t.\t.\t.\t.\t.\t.\t.";
				
					if (!$kall{$ktemp}){$kall{$ktemp}=1;push @ktrack,$ktemp;}
                                        if (!$kintron{$ktemp}){$kintron{$ktemp}=$cur;}
                                        else {$kintron{$ktemp}="$kintron{$ktemp},$cur";}
				}	
				#last;
			}
		}
		if (@ktrack ne undef){
			# I could have combined everything before but this way I can keep track of each feature separately			
			foreach (@ktrack){
				if ($kcds{$_} ne undef){print SAVE "$_\t$kcds{$_}\n";}
				elsif ($kutr5{$_} ne undef){print SAVE "$_\t$kutr5{$_}\n";}
				elsif ($kutr3{$_} ne undef){print SAVE "$_\t$kutr3{$_}\n";}
				elsif ($kexon{$_} ne undef){print SAVE "$_\t$kexon{$_}\n";}
				elsif ($kintron{$_} ne undef){print SAVE "$_\t$kintron{$_}\n";}
			}
		}
		else {print "Error\n$y\n";exit;}
		
		$x++;

		if ($x%1e3==0){print " Parsing line $x\n";}
	}

}
close (OPEN);
close (SAVE);


system ("grep 'CDS' $output > $temp/temp");
system ("awk '{print \$3}' $temp/temp | uniq -d > $project\_SNPs_within_CDS_with_multiple_outcomes.txt");
system ("awk '{print \$3}' $temp/temp > $project\_SNPs_within_CDS.txt");
system ("rm $temp/temp");
