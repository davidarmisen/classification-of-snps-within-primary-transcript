# Classification of SNPs within primary transcript

Perl script that classifies SNPs within primary transcript according to their position (UTR5', UTR3', exon or intron) and its impact on protein sequence (synonymous, non-synonymous  or stop).

It needs a sequences fasta file, a GTF and a VCF file. Optionally we can use a GFF3 to parse frames data only.

As outputs we obtain:

*  A VCF file containing original VCF information plus 4 new columns: SNPs position (UTR5', UTR3', exon or intron), reference codon, variant codon, reference aminoacid, variant aminoacid, synonymous/non-synonymous/stop mutation, frame , case (internal use), transcript name
*  A text file describing all SNPs within CDS with multiple outcomes, i.e. multiple variants
*  A text file describing all SNPs within CDS

Options:

-project = "Project_name" : It will be used to name all the storable and output files. Default: "new_project".

-fasta = "fasta_file.fa" : Path to fasta file containing chromosome sequences. 

-gtf = "gtf.file" : Path to gtf annotation file containing features coordinates. We use GTF format because it clearly distinguishes UTR from coding exons.

-vcf = "vcf.file" : Path to vcf file containing the SNPs we want to classify.

-temp = "temp_folder" : Path to temporary folder where storable data will be writen. It must be created by the user and it must have write permisions. Defaul: Current folder.

-output = "output.vcf" : Output vcf file name. Default: "$project.snp.classification.vcf"


Other options (optional):

-gff3 = "gff3.file" : Path to gff3 file containing features dataframes. Option added because in our case GTF file contained some dataframe errors.

-frames_to_use = gff3|gtf : Define which frames data to use: 'gff3' or 'gtf'

-store : Decide whether we want to store the intermediate files or not. This will greatly improve reading speed in successive iterations. Recommended if we want to test multiple vcf files.

-use_stored : If the script has already been ran once, all the storable files have been created and we want to use those to test new vcf files.

-verbose : Print extra information

-help : Print this help

